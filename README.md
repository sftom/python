# Python

[![pipeline status](https://gitlab.com/sftom/python/badges/master/pipeline.svg)](https://gitlab.com/sftom/python/-/commits/master)

[![coverage report](https://gitlab.com/sftom/python/badges/master/coverage.svg)](https://gitlab.com/sftom/python/-/commits/master)

[Browse](https://gitlab.com/sftom/python/builds/artifacts/master/browse?job=compile_pdf) the Artifacts or [download](https://gitlab.com/sftom/python/-/jobs/artifacts/master/raw/examples/main.pdf?job=compile_pdf) the PDF directly.