Ambientes para programação
==========================

O seu "ambiente para programação" é o computador no qual você trabalha e todo o software instalado no computador que ajuda a escrever e executar programas. Alguns sistemas são melhores para programação do que outros, mas o melhor sistema para aprender é provavelmente o que você está usando agora. Esta seção ajudará você a criar um sistema que permitirá que você comece a escrever programas rapidamente.

Preferencialmente, utilizaremos o ambiente `Google Colab <https://colab.research.google.com/>`_, por apresentar inúmeras vantagens: não precisaremos instalar absolutamente nada; teremos a vatagem de consultar a atividade em qualquer lugar, e teremos à disposição ótimos recursos.

Visão global
------------

Nosso objetivo é ajudar você a colocar o Python em funcionamento no seu computador, para que você possa escrever e executar seus próprios programas. Para fazer isso, queremos:

Descubra se o Python já está instalado no seu computador.
Instale o Python, se ainda não estiver instalado.

Aviso
-----

Se você já tem uma instalação Python em funcionamento, sinta-se à vontade para pular este notebook.

Distribuição Python Anaconda
----------------------------

Usaremos o Python 3.X incluído na distribuição `Anaconda <https://store.continuum.io/cshop/anaconda/>`_ pela `Continuum Analytics <http://www.continuum.io/>`_).

O Anaconda é uma distribuição Python completamente livre e pronta para uso corporativo para processamento de dados em larga escala, análise preditiva e computação científica.

Além disso, o Anaconda vem com instaladores fáceis de usar para quase todas as plataformas, o que reduz drasticamente a carga de configuração do ambiente (exemplo: `Windows <http://continuum.io/downloads#34>`_).

Guia rápido
-----------

Se você instalou o Anaconda, mas não sabe o que fazer, por favor, dê uma olhada no `Guia de Início Rápido <https://store.continuum.io/static/img/Anaconda-Quickstart.pdf>`_.

Distribuições padrão
--------------------

Se você preferir instalar distribuições mais "clássicas" do Python (por exemplo, aquelas disponíveis no site python.org), por favor, dê uma olhada neste notebook:

`Programming Environment <http://nbviewer.ipython.org/urls/raw.github.com/ehmatthes/intro_programming/master/notebooks/programming_environment.ipynb>`_.