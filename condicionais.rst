Estruturas condicionais
=======================

Se as declarações em Python nos permitem dizer ao computador para executar ações alternativas com base em um determinado conjunto de resultados.

Verbalmente, podemos imaginar que estamos dizendo ao computador:

"Ei, se este caso acontecer, execute alguma ação"

Podemos, então, expandir ainda mais a ideia com instruções elif e else, que nos permitem dizer ao computador:

"Ei, se este caso acontecer, execute alguma ação. Caso outro caso ocorra, realize alguma outra ação. Outra coisa - nenhum dos casos acima aconteceu, execute esta ação"

Vamos em frente e veja o formato da sintaxe para instruções if para ter uma ideia melhor disso:

    if case1:
        executar ação 1
    elif case2:
        executar ação 2
    else: 
        executar ação 3

Condicional if
--------------

Vamos ver um exemplo rápido disso:

No código a seguir temos um exemplo de uso do if no qual verificamos se a variável idade é menor que 20.

.. code-block:: python
   :linenos:
   :caption: Estrutura condicional if

   sentenca = True

   if sentenca:
       print('sentenca verdadeira')
   
   if not sentenca:
    print('sentenca nao-verdadeira')

.. code-block:: python
   :caption: Uma lista vazia é convertida implicitamente para um valor booleano (*bool*).

   lista_vazia = []

   if lista_vazia:
      print('A lista vazia nao sera avaliada como True')


.. code-block:: python
   :caption: A variável valor está entre 0 e 1 ou valor é igual a 3.

   valor = 3

   if 0 <= valor < 1 or valor == 3:
      print('valor está entre [0,1[ ou valor = 3')

.. code-block:: python
   :caption: A variável idade recebe o inteiro 18. Em seguida, caso a idade seja menor que 20, a String 'Você é jovem!' será exibida.

   idade = 18
   if idade < 20:
      print('Você é jovem!')

if-else
-------

Vimos anteriormente como utilizar o if para executar uma ação caso uma condição seja atendida. No entanto, nenhum comportamento específico foi definido para o caso de a condição não ser satisfeita. Quando isso é necessário, precisamos utilizar a reservada else.

.. code-block:: python
   :linenos:
   :caption: Estrutura condicional if

   meu_dicionario = {}

   if meu_dicionario:
      print('Ha alguma coisa no meu dicionario')
   else:
      print('O meu dicionario está vazio')

