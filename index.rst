.. sftom-sphinx documentation master file, created by
   sphinx-quickstart on Thu Nov  7 09:25:10 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bem-vindo ao Python!
====================

.. toctree::
   :maxdepth: 2
   :caption: Conteúdos:

   introducao
   condicionais

Índices e tabelas
=================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
